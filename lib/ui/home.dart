import 'package:flutter/material.dart';

_tappClass(){
  debugPrint("tapped on class");
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //return Center(child: Text("Test", textDirection: TextDirection.ltr,),);
//    return Center(
//      child: Text(
//        "Hello world",
//        textScaleFactor: 5,
//        textDirection: TextDirection.ltr,
//      ),
//    );

    return Material(
      color: Colors.blueAccent,
      child: Center(
        child: Text(
          "Hello World",
          textDirection: TextDirection.ltr,
          style: TextStyle(
            fontSize: 40,
            fontWeight: FontWeight.w500,
            fontStyle: FontStyle.italic,
          ),
        ),
      ),
    );
  }
}

class ScaffoldExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Scaffold",
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.w600,
            fontStyle: FontStyle.normal,
            color: Colors.yellow.shade500,
          ),
        ),
        centerTitle: false,
        backgroundColor: Colors.blueAccent,
        actions: <Widget>[
          IconButton(icon: Icon(Icons.email), onPressed: () => debugPrint("tapped")),
          IconButton(icon: Icon(Icons.cake), onPressed: () => _tappClass())
        ],
      ),
      body: Center(
        child: Text("Hello", style: TextStyle(fontSize: 20),),
      ),
    );
  }
}
