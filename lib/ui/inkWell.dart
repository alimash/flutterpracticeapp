import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter1_app/ui/gestureDetector.dart';

_alarm() {
  debugPrint("Alarm Tapped");
}

class inkWell extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "HiApp",
          style: TextStyle(),
        ),
        backgroundColor: Colors.black,
        actions: <Widget>[
          IconButton(icon: Icon(Icons.alarm), onPressed: _alarm)
        ],
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CustomButton(),

            InkWell(
              child: Text(
                "Tape ME!",
                style: TextStyle(fontSize: 23.4),
              ),
              onTap: () => debugPrint("Test"),
            ),
            InkWell(
              child: Text(
                "Tap ME as well",
                style: TextStyle(fontSize: 23.4),
              ),
              onTap: () => debugPrint("Test2"),
            )
          ],
        ),
      ),
    );
  }
}
