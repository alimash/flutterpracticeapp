import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter1_app/ui/gestureDetector.dart';

class ButtonCreator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(

    );
  }
}


class ShowOffApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(12, 13, 23, 12),
        title: Text(
          "Totally a New App",
          style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: Colors.blueAccent),
        ),
      ),
      body: Container(
        alignment: Alignment.topLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            InkWell(
              child: Text(
                "New Button",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              onTap: () => debugPrint("1st Button Pressed!"),
            ),
            InkWell(
              child: Text(
                "Totally Another Button",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              onTap: () => debugPrint("2ed Button Pressed!"),
            )
          ],
        ),
      ),
      backgroundColor: Colors.greenAccent,
    );
  }
}
