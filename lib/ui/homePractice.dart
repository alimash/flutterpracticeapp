import 'dart:ui';

import 'package:flutter/material.dart';

_callButton() {
  debugPrint("Call button pressed");
}

_moreButton() {
  debugPrint("More button pressed");
}

class HomePracticeScaffold extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Practice App",
          style: TextStyle(
            color: Colors.green,
            fontSize: 23,
            fontWeight: FontWeight.w400,
          ),
        ),
        backgroundColor: Colors.black87,
        actions: <Widget>[
          IconButton(icon: Icon(Icons.add_call), onPressed: _callButton),
          IconButton(icon: Icon(Icons.more), onPressed: _moreButton)
        ],
      ),
      body: Center(
          child: Text(
        "Test",
        style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w300,
            fontStyle: FontStyle.italic),
      )),
    );
  }
}
